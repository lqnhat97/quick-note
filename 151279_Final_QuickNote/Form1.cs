﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _151279_Final_QuickNote
{
    
    public partial class fQuickNote : Form
    {
        
        public fQuickNote()
        {
            InitializeComponent();

            LowLevelKeyboardHook kbh = new LowLevelKeyboardHook();
            kbh.OnKeyPressed += kbh_OnKeyPressed;
            kbh.OnKeyUnpressed += kbh_OnKeyUnPressed;
            kbh.HookKeyboard();

            this.components = new System.ComponentModel.Container();
            this.contextMenu1 = new System.Windows.Forms.ContextMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();

            // Initialize contextMenu
            this.contextMenu1.MenuItems.AddRange(
                        new System.Windows.Forms.MenuItem[] { this.menuItem1 });
            this.contextMenu1.MenuItems.AddRange(
                       new System.Windows.Forms.MenuItem[] { this.menuItem2 });
            this.contextMenu1.MenuItems.AddRange(
                       new System.Windows.Forms.MenuItem[] { this.menuItem3 });

            // Initialize menuItem1
            this.menuItem1.Index = 0;
            this.menuItem1.Text = "E&xit";
            this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);
            // Initialize menuItem2
            this.menuItem2.Index = 0;
            this.menuItem2.Text = "View Notes";
            this.menuItem2.Click += new System.EventHandler(this.danhSáchCácTagToolStripMenuItem_Click);
            // Initialize menuItem3
            this.menuItem3.Index = 0;
            this.menuItem3.Text = "View Statistics";
            this.menuItem3.Click += new System.EventHandler(this.chartToolStripMenuItem_Click);


            // Set up how the form should be displayed.
            
            // Create the NotifyIcon.
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);

            // The Icon property sets the icon that will appear
            // in the systray for this application.
            notifyIcon1.Icon = new Icon("note.ico");           

            // The ContextMenu property sets the menu that will
            // appear when the systray icon is right clicked.
            notifyIcon1.ContextMenu = this.contextMenu1;

            // The Text property sets the text that will be displayed,
            // in a tooltip, when the mouse hovers over the systray icon.
            notifyIcon1.Text = "Quick Note";
                       

        }
        
        #region METHOD
        //Lấy tags
        public string GetTags() => tb_tags.Text;

        //Lấy nội dung
        public string GetContent() => tb_content.Text;

        //Lấy thời gian
        public string GetTime() => dtp1.Text;

        //Lấy tags và xóa khoảng cách
        public string[] tags()
        {
            string[]tmp = GetTags().Split(',');
            for(int i = 0; i < tmp.Length; i++)
            {
                tmp[i] = tmp[i].Trim();
            }
            return tmp;
        }


        #endregion

        #region EVENTS
        //Hiện thông báo khi thoát
        private void fQuickNote_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Bạn có thật sự muốn thoát khỏi chương trình không?",
                "Thông báo",
                MessageBoxButtons.OKCancel) != System.Windows.Forms.DialogResult.OK)
            {
                e.Cancel = true;
            }

        }

        //Thoát chương trình
        private void exitToolStripMenuItem_Click(object sender, EventArgs e) => Application.Exit();

        //Sự kiện khi click và nút thêm
        private void btn_add_Click(object sender, EventArgs e)
        {
            
            //Kiểm tra xem có nhập gì hay không
            if (GetTags() == "" || GetContent() == "")
            {
                MessageBox.Show("Bạn chưa nhập nội dung hoặc tag!", "Thông báo");
            }
            else
            {
                //Tạo file tagslist chứa danh sách các tag
                foreach (string tag in tags())
                {
                    int flag = 0;
                    //Kiểm tra có tạ hay chưa, nếu chưa tạo thì tạo tagslist
                    if (System.IO.File.Exists("tagslist.txt") == false)
                    {
                        System.IO.File.AppendAllText("tagslist.txt", tag);
                        System.IO.File.AppendAllText("tagslist.txt", "\r\n");
                        continue;
                    }
                    string[] tmp = System.IO.File.ReadAllLines("tagslist.txt");
                    //Kiểm tra xem có tag nào bị trùng không
                    foreach (string item in tmp)
                    {
                        if (tag == item)
                        {
                            flag = 1;
                            break;
                        }
                    }
                    //Không trùng thì thêm vào
                    if (flag == 0)
                    {
                        System.IO.File.AppendAllText("tagslist.txt", tag);
                        System.IO.File.AppendAllText("tagslist.txt", "\r\n");
                    }

                }
                //Ghi nội dung vào file tag tương ứng
                foreach (string filename in tags())
                {
                    string tmp;
                    tmp = filename + ".txt";
                    System.IO.File.AppendAllText(tmp, GetTime());
                    System.IO.File.AppendAllText(tmp, "\r\n");
                    System.IO.File.AppendAllText(tmp, GetContent());
                    System.IO.File.AppendAllText(tmp, "\r\n");
                }
                //Hiện form tags list
                MessageBox.Show("Your note was added", "Quick Note");
                btn_rewrite_Click(sender, e);
            }
        }

        private void txtValue_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btn_add_Click(sender, e);
            }
        }
        
        //Menu -> Tags List
        private void danhSáchCácTagToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fTagsList f = new fTagsList();
            this.Hide();
            f.ShowDialog();
            this.Show();
        }

        // Double click vào icon thì hiện lên
        private void notifyIcon1_DoubleClick(object Sender, EventArgs e)
        {
            this.Show();
            //Đưa về trạng thái bình thường
            this.WindowState = FormWindowState.Normal;
            // Activate the form.
            //this.Activate();
        }

        //Click Exit góc
        private void menuItem1_Click(object Sender, EventArgs e)
        {
            this.Close();
        }


        //Phím tắt
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Control | Keys.Space) && FormWindowState.Minimized == this.WindowState)
            {
                this.Show();
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        //Show nofitication
        private void frmMain_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                notifyIcon1.Visible = true;
               
                //Hiện thông báo ở góc
                this.Hide();
                this.notifyIcon1.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info; 
                this.notifyIcon1.BalloonTipText = "Double click or LWindow + Space to show";
                this.notifyIcon1.BalloonTipTitle = "Nofitication";
                notifyIcon1.ShowBalloonTip(500);
                notifyIcon1.DoubleClick += new System.EventHandler(this.notifyIcon1_DoubleClick);

            }

            else if (FormWindowState.Normal == this.WindowState)
            {
                notifyIcon1.Visible = false;
            }
        }

        //Click button Clear xóa các text box
        private void btn_rewrite_Click(object sender, EventArgs e)
        {
            tb_tags.Clear();
            tb_content.Clear();
        }

        //Menu -> view statistic hiện biểu đồ
        private void chartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fChart f = new fChart();
            this.Hide();
            f.ShowDialog();
            this.Show();
        }

        bool lwinKeyPressed;
        bool spaceKeyPressed;

        void kbh_OnKeyPressed(object sender, Keys e)
        {
            if (e == Keys.LWin)
            {
                lwinKeyPressed = true;
            }
            else if (e == Keys.Space)
            {
                spaceKeyPressed = true;
            }
            CheckKeyCombo();
        }

        void kbh_OnKeyUnPressed(object sender, Keys e)
        {
            if (e == Keys.LWin)
            {
                lwinKeyPressed = false;
            }
            else if (e == Keys.Space)
            {
                spaceKeyPressed = false;
            }
        }

        void CheckKeyCombo()
        {
            if (lwinKeyPressed && spaceKeyPressed)
            {
                this.Show();
                this.WindowState = FormWindowState.Normal;
            }
        }
    }
#endregion
}
