﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _151279_Final_QuickNote
{
    public partial class fChart : Form
    {
        public fChart()
        {
            InitializeComponent();
        }

        

        //Lấy danh sách tags
        public string[] content(string filename)
        {
            return System.IO.File.ReadAllLines(filename);
        }

        //Tính toán và vẽ biểu đồ
        void drawChart()
        {
            lb_tags.Items.Clear();
            float sum = 0.0f;
            int[] iarrPieValues = null;
            int lbValuesCount = 0;
            try
            {
                //Tạo bảng màu
                Color[] color = { Color.Red, Color.Blue, Color.Maroon, Color.Yellow, Color.Green, Color.Indigo,Color.Coral,Color.BurlyWood, Color.AliceBlue, Color.Orange,Color.Red,Color.Pink,Color.Plum,
                                Color.WhiteSmoke,Color.MistyRose,Color.LightCyan,Color.Salmon,Color.DarkCyan,Color.Gold,Color.Tomato,Color.MediumSlateBlue};

                //Đưa danh sách vào listbox
                lbValuesCount = content("tagslist.txt").Length;
                iarrPieValues = new int[lbValuesCount];

                //Đếm số lượng note mỗi tag
                for (int i = 0; i < lbValuesCount; i++)
                {
                    iarrPieValues[i] = content(content("tagslist.txt")[i] + ".txt").Length;
                    sum += iarrPieValues[i];
                }

                Rectangle rect = new Rectangle(10, 10, 225, 225);
                Graphics graphics = pictureBox1.CreateGraphics();
                graphics.Clear(pictureBox1.BackColor);

                float fDegValue = 0.0f;
                float fDegSum = 0.0f;
                float fpercent = 0.0f;

                for (int i = 0; i < iarrPieValues.Length; i++)
                {
                    fDegValue = (iarrPieValues[i] / sum) * 360;
                    fpercent = fDegValue / 360*100;
                    lb_tags.Items.Add("Tag: \""+content("tagslist.txt")[i]+"\" as "+ color[i].ToString()+ ":" + fpercent.ToString()+"%");

                    Brush brush = new SolidBrush(color[i]);
                    graphics.FillPie(brush, rect, fDegSum, fDegValue);
                    fDegSum += fDegValue;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            drawChart();
        }
    }
}
