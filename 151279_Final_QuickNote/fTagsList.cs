﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _151279_Final_QuickNote
{

    public partial class fTagsList : Form
    {
        public fTagsList()
        {
            InitializeComponent();
            LoadTreeview("tagslist.txt");

        }

        //Tạo treeview đọc từ tagslist
        void LoadTreeview(string filename)
        {
            foreach (string item in content(filename))
            {
                TreeNode root = new TreeNode();
                root.Text = item;
                tv_list.Nodes.Add(root);
            }
        }

        //Lấy nội dung từ file
        public string[] content(string filename)
        {
            return System.IO.File.ReadAllLines(filename);
        }

        public string str_tb_content(string content)
        {
            return content;
        }

        //Load các file tag
        void readtext(string filename)
        {
            lv_show.Items.Clear();
            string[] tmp_tag = content(filename);
            ListViewItem listViewItem = new ListViewItem();
            for (int i = 0; i < tmp_tag.Length; i = i + 2)
            {
                listViewItem = new ListViewItem();
                listViewItem.Text = tmp_tag[i];
                string tmp30 = tmp_tag[i + 1];
                int count = tmp30.Length;
                if (count > 30)
                {
                    tmp30 = tmp30.Remove(30, count - 30);
                }
                listViewItem.SubItems.Add(new ListViewItem.ListViewSubItem() { Text = tmp30 });

                lv_show.Items.Add(listViewItem);
            }
        }

        //Hiện danh sách các note khi bấm vào các tag
        private void tv_list_AfterSelect(object sender, TreeViewEventArgs e)
        {
            string NodeName = tv_list.SelectedNode.ToString().Replace("TreeNode: ", String.Empty) + ".txt";
            //MessageBox.Show(NodeName);
            readtext(NodeName);
        }

        //Hiện nội dung đầy đủ
        private void lv_show_SelectedIndexChanged(object sender, EventArgs e)
        {
            string NodeName = tv_list.SelectedNode.ToString().Replace("TreeNode: ", String.Empty) + ".txt";
            ListView listView = sender as ListView;
            foreach (ListViewItem item in listView.SelectedItems)
            {
                tb_content.Text = content(NodeName)[lv_show.Items.IndexOf(item) * 2 + 1];
            }
        }
    }
}
